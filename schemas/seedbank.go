package schemas

import "gorm.io/gorm"

// SeedBank is seed bank
type SeedBank struct {
	gorm.Model
	Name    string `json:"name" gorm:"uniqueIndex"`
	LogoURL string `json:"logoUrl" gorm:"uniqueIndex"`
}
