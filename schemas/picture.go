package schemas

import (
	"time"
)

// Picture is a growing session picture
type Picture struct {
	Base
	FilePath string    `json:"filePath" gorm:"uniqueIndex"`
	TakenAt  time.Time `json:"takenAt"`
}
