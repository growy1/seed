package schemas

// SeedStock is a seed items in stock for a particular strain
type SeedStock struct {
	Base
	Strain   *Strain `json:"strain" gorm:"strain"`
	StrainID uint    `json:"strainId" gorm:"uniqueIndex"`
	Count    float64 `json:"count"`
	Unit     *string `json:"unit"`
}

// DeviceStock is a device item in stock
type DeviceStock struct {
	Base
	Device   *Device `json:"device" gorm:"-"`
	DeviceID uint    `json:"deviceId" gorm:"uniqueIndex"`
	Count    float64 `json:"count"`
	Unit     *string `json:"unit"`
}

// HarvestStock is an harvested plant item in stock
type HarvestStock struct {
	Base
	Plant   *Plant  `json:"plant" gorm:"-"`
	PlantID string  `json:"plantId" gorm:"uniqueIndex"`
	Count   float64 `json:"count"`
	Unit    *string `json:"unit"`
}

// Inventory is a user inventory
type Inventory struct {
	Base
	SeedStock    []SeedStock    `json:"seedStock" gorm:"many2many:inventory_seedstock"`
	DeviceStock  []DeviceStock  `json:"deviceStock" gorm:"many2many:inventory_devicestock"`
	HarvestStock []HarvestStock `json:"harvestStock" gorm:"many2many:inventory_harveststock"`
}
