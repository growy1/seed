package schemas

import (
	"errors"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

// Credential is wip to add external auth service running
type Credential struct {
	Password        *string `json:"password" gorm:"-"`
	PasswordHash    string  `json:"-"`
	OTPSharedSecret *string `json:"-"`
}

// User is a user
type User struct {
	Base
	Name            string           `json:"name" gorm:"uniqueIndex"`
	Role            Role             `json:"role"`
	PasswordHash    string           `json:"-"`
	OTPSharedSecret *string          `json:"-"`
	IconURL         *string          `json:"iconUrl"`
	FCMToken        *string          `json:"fcmToken"`
	Password        *string          `json:"password" gorm:"-"`
	Email           *string          `json:"email" gorm:"uniqueIndex"`
	InventoryID     *string          `json:"inventoryId"`
	Inventory       *Inventory       `json:"inventory" gorm:"-"`
	APIKeys         []APIKey         `json:"apiKeys" gorm:"many2many:user_apikeys"`
	GrowingSessions []GrowingSession `json:"growingSessions" gorm:"many2many:user_growingsessions"`
}

// BeforeCreate salt and hash password before saving
func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	if u.Password == nil {
		return errors.New("missing password")
	}
	bytes, err := bcrypt.GenerateFromPassword([]byte(*u.Password), 14)
	if err != nil {
		return err
	}
	u.PasswordHash = string(bytes)
	u.Base.BeforeCreate(tx)
	return nil
}

// BeforeUpdate salt and hash updated password before saving
func (u *User) BeforeUpdate(tx *gorm.DB) (err error) {
	if u.Password == nil {
		return nil
	}
	bytes, err := bcrypt.GenerateFromPassword([]byte(*u.Password), 14)
	if err != nil {
		return err
	}
	u.PasswordHash = string(bytes)
	return nil
}
