package schemas

import "time"

// APIKey to access growy services
type APIKey struct {
	Base
	User     string     `json:"user"`
	Key      string     `json:"key"`
	ExpireAt *time.Time `json:"expireAt"`
}
