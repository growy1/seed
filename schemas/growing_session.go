package schemas

import (
	"database/sql/driver"
	"time"
)

// GrowingSessionType is a growing session type
type GrowingSessionType string

const (
	// Indoor is an indoor session
	Indoor GrowingSessionType = "INDOOR"
	// Outdoor is an outdoor session
	Outdoor GrowingSessionType = "OUTDOOR"
)

// Scan retrieve a GrowingSessionType value
func (g *GrowingSessionType) Scan(value interface{}) error {
	*g = GrowingSessionType(value.(string))
	return nil
}

// Value return the string value for a GrowingSessionType
func (g GrowingSessionType) Value() (driver.Value, error) {
	return string(g), nil
}

// GrowingSessionState is a growing session state
type GrowingSessionState string

const (
	// Project is a projected growing session
	Project GrowingSessionState = "PROJECT"
	// Running is a running growing session
	Running GrowingSessionState = "RUNNING"
	// Terminated is a finished growing session
	Terminated GrowingSessionState = "TERMINATED"
)

// Scan retrieve a GrowingSessionState value
func (g *GrowingSessionState) Scan(value interface{}) error {
	*g = GrowingSessionState(value.(string))
	return nil
}

// Value return the string value for a GrowingSessionState
func (g GrowingSessionState) Value() (driver.Value, error) {
	return string(g), nil
}

// GrowingSession is a culture session
type GrowingSession struct {
	Base
	Name      string              `json:"name" gorm:"uniqueIndex"`
	Type      GrowingSessionType  `json:"type"`
	State     GrowingSessionState `json:"state"`
	StartedAt time.Time           `json:"startedAt"`
	EndedAt   *time.Time          `json:"endedAt"`
	Alarms    []MeasureAlarm      `json:"alarms" gorm:"many2many:growingsession_measurealarms"`
	BoxVolume []Measure           `json:"boxVolume" gorm:"many2many:growingsession_boxvolume"`
	Plants    []Plant             `json:"plants" gorm:"many2many:growingsession_plants"`
	Devices   []Device            `json:"devices" gorm:"many2many:growingsession_devices"`
	Pictures  []Picture           `json:"pictures" gorm:"many2many:growingsession_pictures"`
	Measures  []Measure           `json:"measures" gorm:"many2many:growingsession_measures"`
}
