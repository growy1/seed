package schemas

import (
	"time"
)

// Plant is a growing cannabis plant
type Plant struct {
	Base
	Name      string        `json:"name" gorm:"uniqueIndex"`
	StartedAt time.Time     `json:"startedAt"`
	EndedAt   *time.Time    `json:"endedAt"`
	StrainID  uint          `json:"strainId"`
	Strain    *Strain       `json:"strain" gorm:"-"`
	HarvestID *string       `json:"harvestId"`
	Harvest   *HarvestStock `json:"harvestStock" gorm:"-"`
	States    []GrowState   `json:"states" gorm:"many2many:plant_grow_states"`
	Pictures  []Picture     `json:"pictures" gorm:"many2many:plant_pictures"`
}
