package schemas

import "database/sql/driver"

// DevelopmentStateType is a developemnt state of a plant
type DevelopmentStateType string

const (
	// Seeding is when plant is still a seed
	Seeding DevelopmentStateType = "SEEDING"
	// Germinating is when a plant start and end germinating
	Germinating DevelopmentStateType = "GERMINATING"
	// Growing is the growth stage
	Growing DevelopmentStateType = "GROWING"
	// Flowering is the last stage before harvest
	Flowering DevelopmentStateType = "FLOWERING"
	// Drying is the dry step for harvested flowers
	Drying DevelopmentStateType = "DRYING"
	// Curing is the curing step after being dried
	Curing DevelopmentStateType = "CURING"
)

// Scan retrieve a straintype value
func (d *DevelopmentStateType) Scan(value interface{}) error {
	*d = DevelopmentStateType(value.(string))
	return nil
}

// Value return the string value for a StrainType
func (d DevelopmentStateType) Value() (driver.Value, error) {
	return string(d), nil
}
