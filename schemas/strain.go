package schemas

import (
	"database/sql/driver"

	"gorm.io/gorm"
)

// Cannabinoid is a cannabinoid molecule
type Cannabinoid struct {
	gorm.Model
	StrainID   uint    `json:"strainId"`
	Name       string  `json:"name"`
	Percentage float64 `json:"percentage"`
}

// StrainType is a cannabis strain type
type StrainType string

const (
	// Indica is an indica strain
	Indica StrainType = "INDICA"
	// Sativa is a sativa strain type
	Sativa StrainType = "SATIVA"
	// Ruderalis is a ruderalis strain type
	Ruderalis StrainType = "RUDERALIS"
)

// Scan retrieve a straintype value
func (s *StrainType) Scan(value interface{}) error {
	*s = StrainType(value.(string))
	return nil
}

// Value return the string value for a StrainType
func (s StrainType) Value() (driver.Value, error) {
	return string(s), nil
}

// StrainTypeInfos is the percentage between sativa / indica and or ruderalis in cannabis plant
type StrainTypeInfos struct {
	gorm.Model
	StrainID   uint       `json:"strainId"`
	Type       StrainType `json:"type"`
	Percentage *float64   `json:"percentage"`
}

// StrainPhotoperiod is a cannabis strain photoperiod type
type StrainPhotoperiod string

const (
	// Regular is an regular strain
	Regular StrainPhotoperiod = "REGULAR"
	// Autoflowering is an autoflowering strain
	Autoflowering StrainPhotoperiod = "AUTOFLOWERING"
)

// Scan retrieve a strainphotoperiod value
func (s *StrainPhotoperiod) Scan(value interface{}) error {
	*s = StrainPhotoperiod(value.(string))
	return nil
}

// Value return the string value for a StrainPhotoperiod
func (s StrainPhotoperiod) Value() (driver.Value, error) {
	return string(s), nil
}

// SeedFinderStrainInfo is seed info from [seed finder api](https://en.seedfinder.eu/api/)
type SeedFinderStrainInfo struct {
	gorm.Model
	SeedFinderID            string  `json:"seedFinderId"`
	SeedFinderBreederID     string  `json:"seedFinderBreederId"`
	SFStrainHTMLDescription *string `json:"sfStrainHtmlDescription"`
}

// Strain is a cannabis strain
type Strain struct {
	gorm.Model
	Supplier               string               `json:"supplier"`
	Link                   string               `json:"link"`
	ImageURL               *string              `json:"imageUrl"`
	SeedFinderStrainInfoID *uint                `json:"seedFinderStrainInfoId"`
	SeedFinderStrainInfo   SeedFinderStrainInfo `json:"seedFinderStrainInfo" gorm:"-"`
	SeedBankID             uint                 `json:"seedBankId" gorm:"uniqueIndex:idx_strain"`
	Name                   string               `json:"name" gorm:"uniqueIndex:idx_strain"`
	Genetic                string               `json:"genetic"`
	Photoperiod            StrainPhotoperiod    `json:"photoperiod"`
	Type                   []StrainTypeInfos    `json:"type" gorm:"many2many:strain_straintypeinfos"`
	Cannabinoids           []Cannabinoid        `json:"cannabinoids" gorm:"many2many:strain_cannabinoids"`
	FloweringTimes         []Measure            `json:"floweringTimes" gorm:"many2many:strain_flowering_times"`
}
