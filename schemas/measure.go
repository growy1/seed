package schemas

import (
	"database/sql/driver"
	"time"
)

// DataType is a sensor physical data type
type DataType string

const (
	// Volume is a volume data type
	Volume DataType = "VOLUME"
	// Power is an electrical power data type
	Power DataType = "POWER"
	// Temperature is a temperature data type
	Temperature DataType = "TEMPERATURE"
	// Humidity is a humidity data type
	Humidity DataType = "HUMIDITY"
	// Height is a height data type
	Height DataType = "HEIGHT"
	// Width is a width data type
	Width DataType = "WIDTH"
	// Length is a legth data type
	Length DataType = "LENGTH"
	// Duration is a day (24H) data type
	Duration DataType = "DURATION"
)

// Scan retrieve a datatype value
func (d *DataType) Scan(value interface{}) error {
	*d = DataType(value.(string))
	return nil
}

// Value return the string value for a Datatype
func (d DataType) Value() (driver.Value, error) {
	return string(d), nil
}

// Measure is a measurement entity
type Measure struct {
	Base
	Type  DataType   `json:"type"`
	Date  *time.Time `json:"date"`
	Value float64    `json:"value"`
	Unit  string     `json:"unit"`
}
