package schemas

import "database/sql/driver"

// Role is a growy app role
type Role string

const (
	// God is all above
	God Role = "GOD"
	// Admin solve problems
	Admin Role = "ADMIN"
	// Advisor can look at some user sessions
	Advisor Role = "ADVISOR"
	// Grower is a common joe
	Grower Role = "USER"
)

// Scan retrieve a role value
func (r *Role) Scan(value interface{}) error {
	*r = Role(value.(string))
	return nil
}

// Value return the string value for a Role
func (r Role) Value() (driver.Value, error) {
	return string(r), nil
}
