package schemas

import "time"

// GrowState represent a plant growing state
type GrowState struct {
	Base
	DevelopmentState DevelopmentStateType `json:"developmentState"`
	StartedAt        time.Time            `json:"startedAt"`
	EndedAt          *time.Time           `json:"endedAt"`
	Comments         *string
}
