package schemas

import (
	"database/sql/driver"

	"gorm.io/gorm"
)

// DeviceType is a cultivation device type
type DeviceType string

const (
	// Monitoring is a volume data type
	Monitoring DeviceType = "MONITORING"
	// Measurement is a measurement device
	Measurement DataType = "MEASUREMENT"
	// Ventilation is a ventialtion device type
	Ventilation = "VENTILATION"
	// Light is a light device type
	Light = "LIGHT"
)

// Scan retrieve a datatype value
func (d *DeviceType) Scan(value interface{}) error {
	*d = DeviceType(value.(string))
	return nil
}

// Value return the string value for a Datatype
func (d DeviceType) Value() (driver.Value, error) {
	return string(d), nil
}

// Device is an indoor growing session device
type Device struct {
	gorm.Model
	Type    DeviceType `json:"type" gorm:"uniqueIndex:idx_device"`
	Name    string     `json:"name" gorm:"uniqueIndex:idx_device"`
	PowerID *uint      `json:"powerId"`
}
