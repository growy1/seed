package schemas

import (
	"gorm.io/gorm"
)

// PlantState is a plant state status
type PlantState struct {
	gorm.Model
	State DevelopmentStateType `json:"state" gorm:"uniqueIndex"`
}

// Nutrient is a cannabis culture nutrient
type Nutrient struct {
	gorm.Model
	Name       string       `json:"name" gorm:"uniqueIndex:idx_nutrient"`
	Nitrogen   uint         `json:"nitrogen" gorm:"uniqueIndex:idx_nutrient"`
	Phosphorus uint         `json:"phosphorus" gorm:"uniqueIndex:idx_nutrient"`
	Potassium  uint         `json:"potassium" gorm:"uniqueIndex:idx_nutrient"`
	ForStates  []PlantState `json:"forStates" gorm:"many2many:nutrient_forplantstates"`
}

// NPK is the slice of the three main component of a nutrient (if any)
func (n *Nutrient) NPK() *[]uint {
	var res *[]uint
	if n.Nitrogen == 0 &&
		n.Phosphorus == 0 &&
		n.Potassium == 0 {
		return nil
	}
	res = &[]uint{
		n.Nitrogen,
		n.Phosphorus,
		n.Potassium,
	}
	return res
}

// NutrientSet is a nutrient set from a particular brand
type NutrientSet struct {
	gorm.Model
	Supplier string     `json:"supplier"`
	Name     string     `json:"name" gorm:"uniqueIndex:idx_nutrientset"`
	Brand    string     `json:"brand" gorm:"uniqueIndex:idx_nutrientset"`
	Elements []Nutrient `json:"elements" gorm:"many2many:nutrientset_nutrients"`
}
