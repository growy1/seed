package schemas

import "github.com/gofrs/uuid"

// MeasureAlarm is the interval on wich a particular measure trigger an alarm
type MeasureAlarm struct {
	Base
	GrowingSessionID uuid.UUID `json:"growingSessionId" gorm:"index:idx_alarmtype,unique"`
	On               DataType  `json:"on" gorm:"index:idx_alarmtype,unique"`
	Low              *Measure  `json:"low" gorm:"-"`
	High             *Measure  `json:"high" gorm:"-"`
	LowID            *string   `json:"lowId"`
	HighID           *string   `json:"highId"`
}
