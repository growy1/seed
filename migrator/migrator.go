package migrator

import (
	"fmt"

	"gitlab.com/growy1/seed/schemas"

	"gorm.io/gorm"
)

// Migrate shemas in sqlite3 database
func Migrate(db *gorm.DB) error {
	var err error
	if db.Dialector.Name() == "sqlite" {
		db.Exec("PRAGMA foreign_keys = ON;")
	}
	if err = db.AutoMigrate(
		schemas.SeedStock{},
		schemas.HarvestStock{},
		schemas.DeviceStock{},
		schemas.Inventory{},
		schemas.User{},
		schemas.APIKey{},
		schemas.Measure{},
		schemas.SeedBank{},
		schemas.Cannabinoid{},
		schemas.SeedFinderStrainInfo{},
		schemas.Strain{},
		schemas.Picture{},
		schemas.GrowState{},
		schemas.Plant{},
		schemas.Device{},
		schemas.GrowingSession{},
		schemas.PlantState{},
		schemas.Nutrient{},
		schemas.NutrientSet{},
	); err != nil {
		return err
	}
	fmt.Printf("[INFO] Successfully migrate schemas\n")
	return nil
}
