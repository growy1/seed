FROM alpine
RUN apk add --update sqlite
RUN mkdir /db
# Create appuser.
ENV USER=appuser
ENV UID=10001 
RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/nonexistent" \    
    --shell "/sbin/nologin" \    
    --no-create-home \    
    --uid "${UID}" \    
    "${USER}"
RUN chown -R appuser:appuser /db

USER appuser:appuser
WORKDIR /db
ENTRYPOINT ["sqlite3"]
CMD ["growy.db"]